// import mongoose  from 'mongoose';
// import P         from 'bluebird';
// import _         from 'lodash';
// import co        from 'co';


// import * as h    from './helper';
// import * as util from './../src/utils/util';
// import * as user from './../src/user/user';
// import * as area from './../src/geo/areaModel';
// import * as hap  from './../src/geo/happeningModel';

// const loc = {
//   rze: [50.04119, 21.99912],
//   rze1: [50.03920, 22.00290],
//   kra: [50.06465,	19.94498]
// };

// describe('Area flows', () => {

//   it('test', (done) => {
//     var userParams1 = { email:'area1@wp.pl', password:'ss' };

//     user.create(userParams1).then((_user) => {
//       var params = {
//         user: _user._id,
//         loc: loc.rze,
//         radius: 2,
//         name: 'test'
//       };

//       P.all([area.create(params), area.create(params)]).then(() => {
//         area.AreaModel.findAsync({user: _user._id}).then((areas) => {
//           h.expect(areas).to.have.length(2);
//         }).then(done);});});});

//   it('create user area for user', (done) => {
//     var params = {loc: loc.rze, radius: 5, name: 'test3'};
//     return h.UserHelper.api('post', '/api/v1/areas', params)
//       .then(body => {
//         h.expect(body).to.have.property('_id');
//         done();
//       }, err => done(err));});

//   it('create two areas, and get them', function(done) {
//     this.timeout(15000);

//     var user = {email: 'damian@wp.pl', password: 'damian'};
//     var user2 = {email: 'damian2@wp.pl', password: 'damian'};

//     var params0 = {loc: loc.rze, radius: 15, name: "Moja dzielnia w Rzeszowie"};
//     var params1 = {loc: loc.rze, radius: 2, name: "Moja dzielnia w Rzeszowie"};
//     var params2 = {loc: loc.kra, radius: 10, name: 'Moja dzielnia w Krakowie'};

//     h.UserHelper.register(user).then(
//       (res) => {
//         h.UserHelper.api('post', '/api/v1/areas', params0, null, null, user);

//         let io1 = h.ioConnect({query: 'token=' + res.body.token});
//         io1.on('userStream:main', function(msg){
//           console.log('msg:', msg);
//         });

//         h.UserHelper.register(user2).then(
//           (res) => {
//             let io2 = h.ioConnect({query: 'token=' + res.body.token});

//             var hapParams = {
//               date: new Date(),
//               loc: loc.rze1,
//               activity: hap.ACTIVITIES[0],
//               description: 'dupa2 dupa2 dupa2 dupa2 dupa2 dupa2'
//             };

//             P.all([
//               h.UserHelper.api('post', '/api/v1/areas', params1, null, null, user2),
//               h.UserHelper.api('post', '/api/v1/areas', params2, null, null, user2),
//               h.UserHelper.api('post', '/api/v1/happenings', hapParams, null, null, user2)
//             ]).then(() => {

//               h.UserHelper.api('get', '/api/v1/areas', null, null, null, user2).then(function(areas) {
//                 h.expect(areas).to.have.length(2);
//                 done();
//               });});

//           });});});


//   it('check generators', function(done) {
//     util.runGenerator(function *() {
//       let userParams = {email: 'damian12@wp.pl', password: 'damian'};
//       let areaParams = {loc: loc.rze, radius: 15, name: "Moja dzielnia w Rzeszowie"};

//       let user = yield h.UserHelper.register(userParams).then(res => res.body);
//       let io1 = yield h.ioConnect({query: 'token=' + user.token});
//       io1.on('userStream:main', function(msg){console.log('msg:', msg);});
//       yield h.UserHelper.api('post', '/api/v1/areas', areaParams, null, null, userParams);

//       let areas = yield h.UserHelper.api('get', '/api/v1/areas', null, null, null, userParams);
//       h.expect(areas).to.have.length(1);

//       done();
//     });
//   });

//   it('check co flow-control goodness generators', function(done) {
//     co(function *() {
//       const userP  = {email: 'damian1234@wp.pl', password: 'damian'};
//       const areaP1 = {loc: loc.rze, radius: 15, name: "Moja dzielnia w Rzeszowie"};
//       const areaP2 = {loc: loc.kra, radius: 15, name: "Moja dzielnia w Krakowie"};

//       yield h.UserHelper.register(userP);
//       yield [
//         h.UserHelper.api('post', '/api/v1/areas', areaP1, null, null, userP),
//         h.UserHelper.api('post', '/api/v1/areas', areaP2, null, null, userP),
//       ];
//       let areas = yield h.UserHelper.api('get', '/api/v1/areas', null, null, null, userP);

//       h.expect(areas).to.have.length(2);
//       done();
//     }).catch(err => {console.error(err);});
//   });

// });
