import express    from 'express';
import http       from 'http';
import bodyParser from 'body-parser';
import morgan     from 'morgan';
import routes     from './routes';

import {cors}         from './utils/util';
import {errorHandler, NotFoundError} from './utils/errorHandler';
import {JWT_SECRET}   from './config';
import expressJwt     from 'express-jwt';
import socket         from './socket';


let app    = express();
let server = http.createServer(app);

// Showing stack errors
app.set('showStackError', true);

app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.all('/*', cors);
app.all('/api/v1/*', expressJwt({secret: JWT_SECRET}));
app.use('/', routes);
app.all("*", (req, res, next) =>
        next(new NotFoundError("404", {message: 'not found'})));

// error handler
app.use(errorHandler);

// socket.io handler
socket.connect(server);


export default server;
