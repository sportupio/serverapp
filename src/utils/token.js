import _       from 'lodash';
import jwt     from 'jsonwebtoken';

import { JWT_SECRET, TOKEN_EXPIRATION } from './../config';
import { UnauthorizedError } from './errorHandler';

//TODO: store it
//TODO: for now simple koko jambo i do przodu
export let create = function(user, req, res, next) {
  let token = jwt.sign({
    _id: user._id,
    email: user.properties.email
  }, JWT_SECRET, { expiresInMinutes: TOKEN_EXPIRATION });

  return {
    token: token
  };
};

export function authorize() {
  return (socket, next) => {
    const token = socket.handshake.query ? socket.handshake.query.token : null;

    if (token) {
      jwt.verify(token, JWT_SECRET, function(err, decoded) {
        if (err) {
          next(new UnauthorizedError('invalid_token', err));
        }
        socket.handshake.user = decoded;
        next();
      });
    } else {
      next(new UnauthorizedError('credentials_required', {message: 'Pleas add token to handshake'}));
    }
  };
}
