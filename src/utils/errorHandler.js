//TODO: re-factor
import winston from 'winston';

const type = {
  UnauthorizedAccessError: 0,
  UnauthorizedError: 1,
  NotFoundError: 2,
  BadRequestError: 3
};

export class UnauthorizedError extends Error {
  constructor(code, error) {
    super(error.message);
    this.name = "UnauthorizedError";
    this.message = error.message;
    this.code = code;
    this.status = 404;
    this.inner = error;
    this.data = {
      message: this.message,
      code: code,
      type: "UnauthorizedError"
    };
  }
};

export class NotFoundError extends Error {
  constructor(code, error) {
    super(error.message);
    this.name = "NotFoundError";
    this.message = error.message;
    this.code = code;
    this.status = 404;
    this.inner = error;
  }
};

export let errorHandler = function(err, req, res, next) {
  winston.log('error', 'errorsUtils:', err);
  let code = 500;
  let msg = { message: err.toString() };
  return res.status(code).json(msg);

  //let errorType = typeof err;
  //let msg = { message: "Internal Server Error" };
  // switch (err.name) {
  // case "UnauthorizedError":
  //   code = err.status;
  //   msg = undefined;
  //   break;
  // case "BadRequestError":
  // case "UnauthorizedAccessError":
  // case "NotFoundError":
  //   code = err.status;
  //   msg = err.inner;
  //   break;
  // default:
  //   break;
  // }
};
