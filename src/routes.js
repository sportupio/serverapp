import express from 'express';

import setUserRoutes  from './user/userRoutes';
import setAreaRoutes  from './area/areaRoutes';
import setEventRoutes from './event/eventRoutes';

let router = express.Router();

setUserRoutes(router);
setAreaRoutes(router);
setEventRoutes(router);


// secure check
router.get('/api/v1/ping', (req, res) =>
           {res.json(req.user.email);});


export default router;
