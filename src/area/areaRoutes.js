import _ from 'lodash';

import * as db   from './../db';
import * as e    from './../utils/errorHandler';
import * as util from './../utils/util';


export default function setAreaRoutes(r) {

  r.post('/api/v1/areas', (req, res, next) => {
    if (!util.isValidStr(req.body.name)) {
      next(new Error('Not valid'));
    }

    db.createArea(
      req.user.email,
      req.body.name,
      req.body.loc[0],
      req.body.loc[1],
      req.body.radius
    ).
      then(val => res.json(val)).
      catch(err => next(new Error(err)));
  });

  r.get('/api/v1/areas', (req, res, next) => {
    db.matchArea(req.user.email)
      .then(val => res.json(val))
      .catch(err => next(new Error(err)));
  });


  // r.put('/api/v1/areas/:areaId', (req, res, next) => {
  //   var params = {
  //     loc: req.body.loc,
  //     radius: req.body.radius,
  //     name: req.body.name
  //   };
  //   area.update(req.params.areaId, req.user._id, params)
  //     .then(area => res.json(area))
  //     .catch(err => next(new Error(err)));
  // });

  // r.delete('/api/v1/areas/:areaId', (req, res, next) => {
  //   area.remove(req.params.areaId, req.user._id)
  //     .then(area => res.json(area))
  //     .catch(err => next(new Error(err)));
  // });

  // r.get('/api/v1/areas', (req, res, next) => {
  //   if (req.query.hap === 'all') {
  //     area.getAllHappeningsFromAllAreasByUserId(req.user._id, req.query)
  //       .then(areas => res.json(areas))
  //       .catch(err => next(new Error(err)));
  //   } else {
  //     area.getAllByUserId(req.user._id)
  //       .then(area => res.json(area))
  //       .catch(err => next(new Error(err)));
  //   }
  // });

  // r.get('/api/v1/areas/:areaId', (req, res, next) => {
  //   area.getOneById(req.params.areaId)
  //     .then(hap => res.json(hap))
  //     .catch(err => next(new Error(err)));
  // });

}
