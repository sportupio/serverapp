import _ from 'lodash';

import * as userModel  from './userModel';
import * as token      from './../utils/token';
import * as e          from './../utils/errorHandler';
import * as util       from './../utils/util';

let omitFromUserProps = user =>
      _.omit(user, ['password']);

export default function setUserRoutes(r) {

  /*
   * POST: /register
   * body: {email: 'piecyk@gmail.com', name: 'piecyk', password: 'test'}
   *
   * response:
   * body: {
   *   created: '2015-06-29T10:34:50.694Z',
   *   name: 'piecyk',
   *   email: 'piecyk@gmail.com',
   *   token: 'eyJ0eXAi...'
   * }
   */
  r.post('/register', (req, res, next) => {
    if (!util.isValidEmail(req.body.email) || !util.isValidStr(req.body.password)) {
      next(new Error('Not valid'));
    }

    userModel.createUser(req.body.email, req.body.name, req.body.password).
      then(
        val => res.json(_.assign(omitFromUserProps(val.properties), token.create(val))),
        err => next(new Error(err))
      );
  });

  /*
   * POST: /api/v1/follow/user
   * body: {email: 'piecyk@gmail.com', password: 'test'}
   *
   * response:
   * body: {
   *   created: '2015-06-29T10:34:50.694Z',
   *   name: 'piecyk',
   *   email: 'piecyk@gmail.com',
   *   token: 'eyJ0eXAi...'
   * }
   */
  r.post('/login', (req, res, next) => {
    if (!util.isValidEmail(req.body.email) || !util.isValidStr(req.body.password)) {
      next(new Error('Not valid'));
    }

    userModel.matchUserByEmailPassword(req.body.email, req.body.password).
      then(
        val => res.json(_.assign(omitFromUserProps(val.properties), token.create(val))),
        err => next(new e.UnauthorizedError('login', err)));
  });


  /*
   * GET: /emailavailable?email=xxx@gmail.com
   *
   * response:
   * body: {status: 'ok'}
   */
  r.get('/emailavailable', (req, res, next) => {
    userModel.getUser(req.query.email).
      then(
        val => next(new Error('email not available')),
        err => res.json({status: 'ok'})
      );
  });

  /*
   * POST: /api/v1/follow/user
   * body: {email: 'piecyk@gmail.com'}
   *
   * response:
   * body: {status: 'ok'}
   */
  r.post('/api/v1/follow/user', (req, res, next) => {
    userModel.followUser(req.user.email, req.body.email).
      then(
        val => res.json({status: 'ok'}),
        err => next(new Error(err))
      );
  });


  //TODO: check this
  // r.put('/api/v1/users', (req, res, next) => {
  //   //TODO: fix this
  //   if (req.query.follow) {
  //     userModel.follow(req.user._id, req.query.follow)
  //       .then(hap => res.json(hap)).catch(err => next(new Error(err)));
  //   } else if (req.query.unfollow) {
  //     userModel.unfollow(req.user._id, req.query.unfollow)
  //       .then(hap => res.json(hap)).catch(err => next(new Error(err)));
  //   } else {
  //     userModel.update(req.user._id, req.body).then(user => {
  //       res.json(omitFromUserProps(user));
  //     }).catch(err => next(new Error(err)));
  //   }
  // });

  // r.get('/api/v1/users', (req, res, next) => {
  //   P.all([
  //     userModel.findOneById(req.user._id),
  //     areaModel.getAllByUserId(req.user._id),
  //     hapModel.getAllByUserId(req.user._id)
  //   ]).then((all) => res.json(_.assign(omitFromUserProps(all[0]), {
  //     areas: all[1],
  //     happening: all[2]
  //   }))).catch(err => next(new Error(err)));
  // });

  // r.get('/api/v1/users/:userId', (req, res, next) => {
  //   P.all([
  //     userModel.findOneById(req.params.userId),
  //     areaModel.getAllByUserId(req.params.userId),
  //     hapModel.getAllByUserId(req.params.userId)
  //   ]).then((all) => res.json(_.assign(omitFromUserProps(all[0]), {
  //     areas: all[1],
  //     happening: all[2]
  //   }))).catch(err => next(new Error(err)));
  // });
}
