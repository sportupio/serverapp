import Q       from 'q';
import _       from 'lodash';
import socket  from './../socket';
import * as db from './../db';


export let createUser = (email, name, password) =>
  db.cypher('CREATE (n:User {data}) RETURN n',
         {email, name, password, created: new Date().toISOString()}).
  then(user => user[0].n);

export let matchUserByEmailPassword = (email, password) =>
  db.cypher(`MATCH (n:User {email: '${email}', password: '${password}'}) RETURN n`).
  then(res => _.isEmpty(res) ? Q.reject('Wrong email or password') : res[0].n);

export let matchUserByEmail = (email) => db.cypher(`MATCH (n:User {email: '${email}'}) RETURN n`).
  then(res => _.isEmpty(res) ? Q.reject('User not found') : res[0].n);

export let matchUserById = (id) => db.cypher(`MATCH (n:User) where id(n) = ${id} RETURN n`).
  then(res => _.isEmpty(res) ? Q.reject('User not found') : res[0].n);

export let followUser = (u1, u2) => {
  return matchUserByEmail(u2).
    then(db.cypher([
      `MATCH (u1:User {email:'${u1}'}), (u2:User {email:'${u2}'})`,
      'CREATE (u1)-[:FOLLOW_USER]->(u2)'
    ].join('\n')).
         then(Q.all([
           db.createNotification(u1, `You started to follow user ${u2}`),
           db.createNotification(u2, `User ${u1} is following you`),
         ])).
         then(socket.notifi));
};

export let unFollowUser = (u1, u2) =>
  db.cypher([
    `MATCH (u1:User {email:'${u1}'}) -[r:FOLLOW_USER]->(u2:User {email:'${u2}'})`,
    'DELETE r'
  ].join('\n'));

export let matchFollowUser = (user) =>
  db.cypher(`MATCH (n:User)-[:FOLLOW_USER]->(u:User{email:'${user}'}) RETURN n`);
