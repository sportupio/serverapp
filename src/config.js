export const HTTP_PORT = process.env.PORT || '3033';

export const NEO4J_URI = process.env['NEO4J_URL'] ||
  process.env['GRAPHENEDB_URL'] ||
  'http://localhost:7474';

export const JWT_SECRET = 'shhhhhhared-secret';
export const TOKEN_EXPIRATION = 60;
export const TOKEN_EXPIRATION_SEC = TOKEN_EXPIRATION * 60;

//heroku config:set NODE_ENV="production"
export const env = process.env.NODE_ENV || 'dev';
