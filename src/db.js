import { NEO4J_URI } from './config';
import Q             from 'q';
import neo4j         from 'neo4j';
import request       from 'request';
import _             from 'lodash';
import socket        from './socket';


const db = new neo4j.GraphDatabase(NEO4J_URI);

export let cypher = function(query, params) {
  let deferred = Q.defer();
  db.cypher({query, params: {data: params}}, function (err, res) {
    if (err) { return deferred.reject(err); }
    return deferred.resolve(res);
  });
  return deferred.promise;
};

export let http = function(url, body, method='POST') {
  let deferred = Q.defer();
  let options = {
    url: NEO4J_URI + url, body: body, method: method, json: true,
    headers: {'content-type': 'application/json', 'accept': 'application/json; charset=UTF-8'}
  };
  request(options, function(err, res) {
    if (err) { return deferred.reject(err); }
    return deferred.resolve(res.body);
  });
  return deferred.promise;
};


//noti
export let createNotification = (user, text) =>
  cypher(`CREATE (n:Notification {data}) RETURN n`,
         {user, text, seen: false, created: new Date().toISOString()}).
  then(res => _.pluck(res, 'n'));

export let matchNotification = (user) =>
  cypher(`MATCH (n:Notification{user:'${user}'}) RETURN n ORDER BY n.created DESC LIMIT 20`).
  then(res => _.pluck(res, 'n'));

export let seenNotification = (id, seen) =>
  cypher(`MATCH (n:Notification) where id(n) = ${id} SET n.seen = ${seen} RETURN n`).
  then(res => _.pluck(res, 'n'));


//area
export let createArea = (user, name, lat, lon, r) =>
  cypher([
    `MATCH (u:User{email:'${user}'})`,
    'CREATE (n:Area {data})',
    'CREATE (u)-[:FOOLLOW_AREA]->(n)',
    'RETURN n'
  ].join('\n'), {user, name, lat, lon, r, created: new Date().toISOString()}).
  then(res =>
       http('/db/data/index/node/area', {'value':'dummy','key':'dummy','uri':`/db/data/node/${res[0].n._id}`}).
       then(() => res[0].n));

export let matchArea = (user) =>
  cypher(`MATCH (u:User{email:'${user}'})-[:FOOLLOW_AREA]->(n) WHERE n.user=u.email RETURN n`).
  then(res => _.pluck(res, 'n'));

export let areaEvents = (areas) => {
  //TODO: lat, lon, r need to be float
  var events = _.reduce(areas, function(result, val, key) {
    return result.concat([
      cypher(`START n=node:event("withinDistance:[${val.properties.lat}, ${val.properties.lon}, ${val.properties.r}]") RETURN n`).
        then(res => _.pluck(res, 'n'))
    ]);
  }, []);
  return Q.all(events);
};

export let matchAreaEvents = (user) =>
  matchArea(user).then(areaEvents).then(res => _.reject(_.uniq(_.flatten(res), '_id'), _.isEmpty));
