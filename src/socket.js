import socketio    from 'socket.io';
import _           from 'lodash';
import {authorize} from './utils/token';
import * as db     from './db';

const Chan = {
  UserStreamMain: 'userStream:main',
  UserStreamMainSeen: 'userStream:main:seen'
};

export default (function () {

  let io, sockets=[], notification={};

  function send(socket) {
    const email = socket.handshake.user.email;

    db.matchNotification(email).then(res => {
      let toSend = (() => {
        if (!notification[email]) {
          notification[email] = res;
          return notification[email];
        } else {
          let data = _.difference(res, notification[email]);
          notification[email] = res;
          return data;
        }
      })();
      socket.emit(Chan.UserStreamMain, toSend);
    });
  }

  function seen(data) {
    db.seenNotification(data.id, true);
  }

  function disconnect(socket) {
    sockets = _.filter(sockets, s => s !== socket);
    notification = _.omit(notification, socket.handshake.user.email);
  }

  return {
    notifi: () => _.each(sockets, send),
    connect: (server) => {
      io = socketio.listen(server);
      io.use(authorize());
      io.on('connection', socket => {
        sockets.push(socket);
        send(socket);
        socket.on(Chan.UserStreamMainSeen, seen);
        socket.on('disconnect', disconnect.bind(null, socket));
      });
    }
  };
})();
