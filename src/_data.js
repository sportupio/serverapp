import co       from 'co';
import _        from 'lodash';
import {env} from './config';

import * as db         from './db';
import * as userModel  from './user/userModel';
import * as eventModel from './event/eventModel';

// then(
// (res) => console.log('res = ', res),
// (err) => console.log('err = ', err)
// );

co(function *() {

  if (env === 'dev') {
    // clear
    yield db.cypher('MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r');
  }

  //TODO: add more CONSTRAINT
  yield db.cypher('CREATE INDEX ON :User(emial)');
  yield db.cypher('CREATE INDEX ON :Notification(user)');

  yield db.cypher('CREATE CONSTRAINT ON (user:User) ASSERT user.email IS UNIQUE');
  yield db.http('/db/data/index/node', {"name":"area", "config":{"provider":"spatial", "geometry_type":"point", "lat":"lat", "lon":"lon"}});
  yield db.http('/db/data/index/node', {"name":"event", "config":{"provider":"spatial", "geometry_type":"point", "lat":"lat", "lon":"lon"}});


  if (env === 'dev') {
    // test data
    yield userModel.createUser('piecyk@gmail.com', 'piecyk', 'test');
    yield userModel.createUser('tomek@gmail.com', 'tomek', 'test');
    yield userModel.createUser('test@gmail.com', 'test', 'test');

    yield userModel.followUser('piecyk@gmail.com', 'tomek@gmail.com');
    yield userModel.followUser('piecyk@gmail.com', 'test@gmail.com');
    yield userModel.followUser('test@gmail.com', 'tomek@gmail.com');
    // yield userModel.followUser('test@gmail.com', 'xxx@gmail.com');

    yield userModel.unFollowUser('piecyk@gmail.com', 'xxx@gmail.com');

    yield db.createArea('tomek@gmail.com', 'Rzeszow', 11.01, 11.02, 100.1);
    yield db.createArea('tomek@gmail.com', 'Rzeszow Borek', 12.005, 11.05, 10.1);

    yield eventModel.createEvent('tomek@gmail.com', 'Bieganie wieczorem', 'RUN', 12.005, 11.05, new Date().toISOString());
    yield eventModel.createEvent('tomek@gmail.com', 'Dupcing wieczorem', 'SEX', 12.005, 11.05, new Date().toISOString()).then(function(event) {
      eventModel.followEvent('piecyk@gmail.com', event._id);
    });
    yield eventModel.createEvent('piecyk@gmail.com', 'Plywanie wieczorem', 'SWIM', 11.005, 11.05, new Date().toISOString());

    yield db.matchAreaEvents('tomek@gmail.com');
  }

}).then(function () {
  console.log('end of dev _data');
}, function (err) {
  console.error(err.stack);
});
