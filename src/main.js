import server       from './app';
import { HTTP_PORT} from './config';
import * as db      from './db';

//TODO: only in dev
import _data from './_data';


server.listen(HTTP_PORT, () => {
  console.log(`Listening on port ${HTTP_PORT}...`);
});
