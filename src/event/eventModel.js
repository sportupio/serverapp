import Q       from 'q';
import _       from 'lodash';

import socket         from './../socket';
import * as db        from './../db';
import * as userModel from './../user/userModel';

//event
export const EventType = [
  'Cycling',
  'Hiking',
  'Kayaking',
  'Running',
  'Sailing',
  'Scuba Diving',
  'Skating',
  'Spear Fishing',
  'Swimming',
];

export let createEvent = (user, name, type, lat, lon, date, description) =>
  db.cypher([
    `MATCH (u:User{email:'${user}'})`,
    'CREATE (n:Event{data})',
    'CREATE (u)-[:FOOLLOW_EVENT]->(n)',
    'RETURN n'
  ].join('\n'), {user, name, type, lat, lon, date, active: true, description, created: new Date().toISOString()}).
  then(res =>
       db.http('/db/data/index/node/event', {'value':'dummy','key':'dummy','uri':`/db/data/node/${res[0].n._id}`}).
       then(() => {
         createdEventNotification(user, res[0].n._id, 'app:event:created');
         return res[0].n;
       }));

export let matchEvent = (user) =>
  db.cypher(`MATCH (u:User{email:'${user}'})-[:FOOLLOW_EVENT]->(n) WHERE n.user=u.email RETURN n`).
  then((res) => _.pluck(res, 'n'));

export let followEvent = (user, id) =>
  db.cypher([
    `MATCH (u:User {email:'${user}'}), (e:Event) where id(e) = ${id}`,
    'CREATE (u)-[:FOLLOW_EVENT]->(e) RETURN e'
  ].join('\n')).
  then(function(event) {
    return Q.all([
      db.createNotification(user, `You started to follow event!`),
      db.createNotification(event[0].e.properties.user, `User ${user} is following your event`),
    ]);
  }).
  then(socket.notifi);


export let unFollowEvent = (user, id) =>
  db.cypher([
    `MATCH (u1:User {email:'${user}'}) -[r:FOLLOW_EVENT]->(e:Event) where id(e) = ${id}`,
    'DELETE r'
  ].join('\n'));

// very noobish why... https://www.youtube.com/watch?v=VRwjSnl-gG8
function createdEventNotification(user, id, type, text) {
  // stworzylem event
  var run = [
    // need to notifi followers of user
    userModel.matchFollowUser(user).then(res => _.pluck(res, 'n.properties.email')),
    // need to notifi users that event is inside his/her areas
    // in future check if we can get shape of country/state from google Geocoder, when creat area check this the we can filter by label state where is event
    db.cypher(`MATCH (n:Area) RETURN n`).
      then(res => _.pluck(res, 'n')).
      then(areas => db.areaEvents(areas).
           then(events => Q.all(events).
                then(res => _.reduce(res, function(r, val, key) {
                  return r.concat(
                    _.contains(_.pluck(val, '_id'), id) ? [areas[key].properties.user] : []
                  );
                }, []))))
  ];

  Q.all(run).then(res => {
    var usersToSend = _.chain(res).
          flatten().
          uniq().
          reject(u => u===user).
          value();

    Q.all(_.map(usersToSend, u => db.createNotification(u, `User: ${user} created event`)).concat([
      // need to notifi user that created the event
      db.createNotification(user, `You just created event`),
    ])).then(socket.notifi);
  });
};
