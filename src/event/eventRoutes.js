import _ from 'lodash';

import * as eventModel from './eventModel';
import * as token      from './../utils/token';
import * as e          from './../utils/errorHandler';
import * as util       from './../utils/util';


export default function setEventRoutes(r) {

  r.get('/api/v1/activities', (req, res, next) => res.json(eventModel.EventType));

  r.post('/api/v1/events', (req, res, next) => {
    if (!util.isValidStr(req.body.name)) {
      next(new Error('Not valid'));
    }

    eventModel.createEvent(
      req.user.email,
      req.body.name,
      req.body.activity,
      req.body.loc[0],
      req.body.loc[1],
      req.body.date,
      req.body.description
    ).
      then(val => res.json(val)).
      catch(err => next(new Error(err)));
  });

  r.get('/api/v1/events', (req, res, next) => {
    eventModel.matchEvent(req.user.email).
      then(val => res.json(val)).
      catch(err => next(new Error(err)));
  });

  // r.put('/api/v1/happenings/:hapId', (req, res, next) => {
  //   var params = {
  //     date: req.body.date,
  //     loc: req.body.loc,
  //     activity: req.body.activity,
  //     description: req.body.description
  //   };
  //   hap.update(req.params.hapId, req.user._id, params)
  //     .then(hap => res.json(hap))
  //     .catch(err => next(new Error(err)));
  // });

  // r.put('/api/v1/happenings/:hapId/register', (req, res, next) => {
  //   hap.register(req.params.hapId, req.user._id)
  //     .then(hap => res.json(hap))
  //     .catch(err => next(new Error(err)));
  // });

  // r.put('/api/v1/happenings/:hapId/follow', (req, res, next) => {
  //   hap.follow(req.params.hapId, req.user._id)
  //     .then(hap => res.json(hap))
  //     .catch(err => next(new Error(err)));
  // });

  // r.delete('/api/v1/happenings/:hapId', (req, res, next) => {
  //   hap.remove(req.params.hapId, req.user._id)
  //     .then(hap => res.json(hap))
  //     .catch(err => next(new Error(err)));
  // });

  // r.get('/api/v1/happenings/:hapId', (req, res, next) => {
  //   hap.getOneById(req.params.hapId)
  //     .then(hap => res.json(hap))
  //     .catch(err => next(new Error(err)));
  // });
}
