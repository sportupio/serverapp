# startupy serverapp

dev flow

* to build run `gulp build`
* to dev run `gulp run`
* to test run `npm test`
* to watch test `npm run testw`

my flow is just to start `npm run testw` then start writing test with it.only(f...) this will run only this test

install mongo

* [piec@piec home]$ mongod

then

* [piec@piec serverapp]$ npm install --python=python2.7
* [piec@piec serverapp]$ npm start


we are using ES6
{"email":"damian@wp.pl", "password":"test"}


# TODO: API

- - -
## Users

### POST /api/v1/users/signup
Create new user

Parameter | Required | Description
--- | --- | ---
email | true | User email
password | true | Chosen password

  :::javascript
  {
    "email": "my.email@mail.com",
    "password": "MySecret"
  }

In case of sign-up error, HTTP response will have status 403 and error message will
be returned:

  :::javascript
  {
    "message": "Email already in use"
  }

Possible error messages:

* **Email already in use**
* **Email is invalid**

In case of successful sign-up newly created user will be returned:

  :::javascript
  {
    "_id": "1234567890",
    "local" : {
      "email": "my.email@mail.com",
      "password": "Hash of given password"
    }
  }

### POST /api/v1/users/login
Login with given credentials. Structure of request and response is the same as
for **signup**.

Possible error messages:

* **User not found**
* **Wrong password**

- - -
## Geolocation

### GET /api/v1/geolocation/near
Get all geolocation objects localized within sphere specified by user.

Parameter | Required | Description
--- | --- | ---
loc | true | Array containing longitude and latitude (WGS84 compliant)
radius | true | Radius determining range of search in kilometers

  :::javascript
  {
    "loc": [123.78, 41.21],
    "radius": 16.8
  }

### GET /api/v1/geolocation/within
Get all geolocations within specified box

Parameter | Required | Description
--- | --- | ---
box | true | Array of WGS84 compliant locations defining box corners

  :::javascript
  {
    "box": [[16.91,45.01],[16.98,44.98]]
  }

- - -
## Activity

### GET /api/v1/activity/list
No parameters, returns list of accepted activities:

  :::javascript
  [
    "Cycling",
    "Hiking",
    "Kayaking",
    "Running",
    "Sailing",
    "Scuba Diving",
    "Skating",
    "Spear Fishing",
    "Swimming"
  ]

- - -
## Happening

### POST /api/v1/happening/create
Creates happening with parameters specified by user

Parameter | Required | Description
--- | --- | ---
date | true | Planned time of happening
description | true | Short description of the happening (5-128 characters)
lng | true | Longitude of happening location
lat | true | Latitude of happening location
activity | true | Activity performed during happening

  :::javascript
  {
    "date": "2015-03-22T14:31:00.000Z",
    "description": "Diving on Cap de Antibes",
    "lng": 7.129654884338379,
    "lat": 43.54623077002694,
    "activity": "Scuba Diving"
  }

In case of successful happening creation, created object is returned with message:

  :::javascript
  {
    "happening": {
      "__v": 0,
      "_type": "Happening",
      "loc": [
        7.129654884338379,
        43.54623077002694
      ],
      "activity": "Scuba Diving",
      "active": true,
      "description": "Diving on Cap de Antibes",
      "date": "2015-03-22T14:31:00.000Z",
      "owner": "5503336699b108d41d661f84",
      "participants": [],
      "comments": [],
      "_id": "550ed24ddcf07048125d7641"
    },
    "message": "Happening created"
  }

In case of error adequate message will be returned, followed with error details:

  :::javascript
  {
    "message": "Validation failed",
    "name": "ValidationError",
    "errors": {
      "date": {
        "message": "Path `date` is required.",
        "name": "ValidatorError",
        "path": "date",
        "type": "required",
        "value": null
      },
      "description": {
        "message": "Path `description` is required.",
        "name": "ValidatorError",
        "path": "description",
        "type": "required",
        "value": ""
      }
    }
  }

- - -
## UserArea

### POST /api/v1/userArea/create

Creates UserArea with parameters specified by user.

Parameter | Required | Description
--- | --- | ---
lng | true | Longitude of UserArea central point
lat | true | Latitude of UserArea central point
radius | true | Radius of area in kilometers [1-50]

  :::javascript
  {
    "lng": 7.129654884338379,
    "lat": 43.54623077002694,
    "radius": "20"
  }

In case of successful creation, created object is returned with message:

  :::javascript
  {
    "userArea": {
      "__v": 0,
      "_type": "UserArea",
      "radius": 20,
      "loc": [
        7.129654884338379,
        43.54623077002694
      ],
      "owner": "5503336699b108d41d661f84",
      "_id": "550ed7d2e2e8e54c12b5e7e4"
    },
    "message": "UserArea created"
  }

In case of error adequate message will be returned, followed with error details:

  :::javascript
  {
    "message": "Validation failed",
    "name": "ValidationError",
    "errors": {
      "radius": {
        "message": "Path `radius` is required.",
        "name": "ValidatorError",
        "path": "radius",
        "type": "required",
        "value": null
      }
    }
  }

- - -
## Comment

### POST /api/v1/comment/create

Creates comment

Parameter | Required | Description
--- | --- | ---
owner | true | Id of object owning the comment (Happening, Picture, etc ...)
parent | false | Id of parent comment, if current is a reply
text | true | Comment text

  :::javascript
  {
    "owner": 550ed7d2e2e8e54c12b5e7e4
    "text": "This is a comment"
  }

In case of successful creation, created object is returned with message:

  :::javascript
  {
    "comment": {
      "posted": "2015-03-30T18:31:00.000Z",
      "author": {
        "id": "5506541299b108d41d661f84",
        "nick": "John.Doe"
      },
      "owner": "550ed7d2e2e8e54c12b5e7e4",
      "parent": null,
      "text": "This is a comment"
      "_id": "5503336699b108d41d661f84"
    },
    "message": "Comment created"
  }

In case of error adequate message will be returned, followed with error details:

  :::javascript
  {
    "message": "Validation failed",
    "name": "ValidationError",
    "errors": {
      "text": {
        "message": "Path `text` is required.",
        "name": "ValidatorError",
        "path": "text",
        "type": "required",
        "value": null
      }
    }
  }
